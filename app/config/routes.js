import React from 'react';
import {Route, Link, Router, IndexRoute, browserHistory} from 'react-router';
import Main from "../components/main";
import Home from '../components/Home';
import TimeLine from '../components/TimeLine';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

var routes = (
    <MuiThemeProvider>
        <Router history={browserHistory}>
            <Route path="/" component={Main}>
                <IndexRoute component={Home} />
                <Route path="/timeline" component={TimeLine} />
            </Route>

        </Router>
    </MuiThemeProvider>
);



export default routes
