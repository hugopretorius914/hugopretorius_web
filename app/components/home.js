import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import SkillChip from './SkillChip';
require('../sass/home.scss');


const SKILLS = [
    {
        skill: "prototyping"
    },
    {
        skill: "mobile apps"
    },
    {
        skill: "ui design"
    },
    {
        skill: "data analysis"
    },
    {
        skill: "wire framing"
    }
]

class Home extends React.Component {


    render() {

        const skills = SKILLS.map((i) =>
            <SkillChip skill={i.skill}/>
        );

        return (
            <div className="home-ct">
                <div className="welcome-message-ct">
                    <div className="welcome-face-ct">
                        <i id="face" className="workandplay welcome-face workandplay-face animated bounceIn"/>
                    </div>
                    <span className="animated fadeIn welcome-message">Hello, I’m a cross platform <span
                        className="welcome-message-bold">ui developer</span> & full stack <span
                        className="welcome-message-bold">designer</span>. Acting at the nexus of code and design to craft hip, smart and authentic brand narrative. Based in Cape Town, South Africa</span>
                    <div className="animated fadeIn social-ct">
                        <FlatButton
                            href="https://za.linkedin.com/in/hugopretorius"
                            target="_blank"
                            secondary={true}
                            icon={<FontIcon className="mdi mdi-linkedin"/>}
                        />
                        <FlatButton
                            href="https://github.com/hugopretorius72"
                            target="_blank"
                            secondary={true}
                            icon={<FontIcon className="mdi mdi-github-box"/>}
                        />
                        <FlatButton
                            href="https://www.behance.net/hugopretorius72"
                            target="_blank"
                            secondary={true}
                            icon={<FontIcon className="mdi mdi-behance"/>}
                        />
                    </div>
                </div>
                <div className="skill-chips">
                    {skills}
                </div>
            </div>
        )
    }
}

export default Home
