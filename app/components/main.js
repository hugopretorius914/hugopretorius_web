/**
 * Created by hugopretorius on 2016/11/27.
 */
import React from 'react';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import AppBar from 'material-ui/AppBar';
require('../sass/main.scss');
import {Route, Link, Router, IndexRoute, browserHistory} from 'react-router';

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: "#36474f",
        primary2Color: "#147A82",
        primary3Color: "#147A82",
        accent1Color: '#147A82',
        accent2Color: "#36474f",
        accent3Color: "#36474f",
        textColor: "#36474f",
        alternateTextColor: "#FFFFFF",
        canvasColor: "#FFFFFF",
        borderColor: "#F1F3F5",
        disabledColor: "#F1F3F5",
        pickerHeaderColor: "blue",
        clockCircleColor: "#36474f",
        shadowColor: "#000000",
    }
});

export default class Main extends React.Component {

    constructor(props) {
        super(props);
        this.state = {open: false};

        this.handleDrawerToggle = () => this.setState({open: !this.state.open});
        this.handleDrawerClose = () => this.setState({open: false});
    }


    render() {
       return (
           <div className="app-ct" >
               <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
                   <AppBar leftIconStyle={{color: 'red'}} style={{backgroundColor: 'transparent', boxShadow: 'none', position:'fixed'}} onLeftIconButtonTouchTap={this.handleDrawerToggle} className="app-bar"/>
               </MuiThemeProvider>
               <Drawer open={this.state.open} docked={false} onRequestChange={(open) => this.setState({open})}>
                   <Link activeStyle={{}} style={{textDecoration: 'none'}} onTouchTap={this.handleDrawerClose} to="/"><MenuItem>hello</MenuItem></Link>
                   <Link activeStyle={{}} style={{textDecoration: 'none'}} onTouchTap={this.handleDrawerClose} to="/timeline"><MenuItem>Menu Item 2</MenuItem></Link>
               </Drawer>
               <MuiThemeProvider muiTheme={getMuiTheme(muiTheme)}>
                 <div className="app-content" >
                   {this.props.children}
                 </div>
               </MuiThemeProvider>
           </div>
       )
    }
}
