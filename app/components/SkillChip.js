import React from 'react';

let styles = {
    chip: {
        backgroundColor: '#e0e0e0',
        height: 32,
        borderRadius: 16,
        margin: "16px 8px 0 0",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: "0 16px"
    },
    chipText: {
        fontFamily: "Gotham",
        fontSize: 14,
        color: '#424242',
        lineHeight: '32px'
    }
};

const SkillChip = ({skill}) => {
    return (
        <div className="chip" style={styles.chip}>
            <span className="chip-text" style={styles.chipText}>
                {skill}
            </span>
        </div>
    )
};

SkillChip.propTypes = {
    skill: React.PropTypes.string.isRequired
};

export default SkillChip;