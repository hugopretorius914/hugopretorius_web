import React from 'react'

require('../sass/timeline.scss');

const numbers = [
    {
        time: 1990,
        description: "I was born this year"
    },
    {
        time: 1997,
        description: "Grad 1 started"
    },
    {
        time: 2004,
        description: "High school freshman"
    },
    {
        time: 2008,
        description: "Matriculated"
    },
    {
        time: 2014,
        description: "First job as UX Designer"
    },
    {
        time: 1990,
        description: "I was born this year"
    },
    {
        time: 1997,
        description: "Grad 1 started"
    },
    {
        time: 2004,
        description: "High school freshman"
    },
    {
        time: 2008,
        description: "Matriculated"
    },
    {
        time: 2014,
        description: "First job as UX Designer"
    }
]


class TimeLine extends React.Component {

    constructor() {
        super();
    }

    componentDidMount() {
        var items = document.querySelectorAll(".timeline li");

// code for the isElementInViewport function

        let isElementInViewport = (el) => {
            var rect = el.getBoundingClientRect();
            return (
                rect.top >= 0 &&
                rect.left >= 0 &&
                rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                rect.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
        }

        for (var i = 0; i < items.length; i++) {
            if (isElementInViewport(items[i])) {
                items[i].classList.add("in-view");
            }
        }

    }

    listenScrollEvent() {
        var items = document.querySelectorAll(".timeline li");

        let isElementInViewport = (el) => {
            var rect = el.getBoundingClientRect();
            return (
                rect.top >= 0 &&
                rect.left >= 0 &&
                rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                rect.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
        }


        for (var i = 0; i < items.length; i++) {
            if (isElementInViewport(items[i])) {
                items[i].classList.add("in-view");
            }
        }

    }

    render() {
        const listItems = numbers.map((number) =>

            <ListItem number={number}/>
        );

        return (
            <div onScroll={this.listenScrollEvent} className="timeLine-ct">
                <section className="intro">
                    <div className="container">
                        <h1>Vertical Timeline</h1>
                    </div>
                </section>
                <section className="timeline">
                    <ul>
                        {listItems}
                    </ul>
                </section>
            </div>
        )
    }
}

export default TimeLine


class ListItem extends React.Component {
    render() {
        return (
            <li className="">
                <div>
                    <time>{this.props.number.time}</time>
                    {this.props.number.description}
                </div>
            </li>
        )
    }
}
